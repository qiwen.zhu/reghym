#!/usr/bin/env bash 

# need to mesh in serial manner for coupling with axisem 
mv DATA/Par_file DATA/Par_file_ref
sed -e "s/^NPROC .*$/NPROC                           = 1/g" DATA/Par_file_ref  > DATA/Par_file

mpirun -np  1   ../../bin/xmeshfem3D 

mv DATA/Par_file_ref DATA/Par_file

../../bin/xdecompose_mesh 8 ./MESH/ ./DATABASES_MPI

