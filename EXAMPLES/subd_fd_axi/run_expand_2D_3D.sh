#!/usr/bin/env bash 


cd axisem_run/SOLVER/RUN1
cp ../../../in_files/coupling_inputs/expand_2D_3D.par ../.
cp ../../../../../EXTERNAL_PACKAGES_coupled_with_SPECFEM3D/AxiSEM_for_SPECFEM3D/UTILS_COUPLING_SpecFEM/xexpand_2D_3D  .
mpirun -np 8  ./xexpand_2D_3D 
cd ../../
