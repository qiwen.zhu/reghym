## Import
import numpy as np
import os
import sys
import math



if __name__ == "__main__":
	
	
	#Read profile
	topo=np.genfromtxt('profil2.topo.smooth')
	y = topo[:,0]
	alt = topo[:,1]
	
	alt_taper = len(y) * [0.]
	y_shift = len(y) * [0.]
	
	#Manually extend profile
	alt_prev = 30*[0.]
	y_prev = 30*[0.]
	alt_post = 111*[0.]
	y_post = 111*[0.]
	
	alt_taper_extend = []
	y_shift_extend = []
	
	for i in range(0,30):
		y_prev[i] = -180.0 + i
	
	for i in range(0,111):
		y_post[i] = 70 + i
	
	
	#Apply the taper on profile and shift the position
	for i in range(0,len(y)):
		y_shift[i] = y[i] - 150.0
		if(y[i] <= 150.):
			alt_taper[i] = alt[i] * math.sin(y[i]/150.*math.pi)
		else:
			alt_taper[i] = 0.0
			
	alt_taper_extend = alt_prev + alt_taper + alt_post
	y_shift_extend = y_prev + y_shift + y_post
	
	#Write tappered and extended profile
	topo_tap=open("profil2.topo.smooth.taper","w")
	for i in range(0,len(y_shift_extend)):
		topo_tap.write("%lf %lf\n"%(y_shift_extend[i],alt_taper_extend[i]))
		
	topo_tap.close()
	
	y_sampled = 361 * [0.]
	
	#Resample profile to fixed step
	for j in range(0,361):
		y_sampled[j] = -180.0 + j
	
	alt_sampled = np.interp(y_sampled,y_shift_extend,alt_taper_extend)
	
	#Write tappered, extended and sampled profile
	topo_tap=open("profil2.topo.samp","w")
	for i in range(0,len(y_sampled)):
		topo_tap.write("%lf %lf\n"%(y_sampled[i],alt_sampled[i]))
		


	#Write Specfem topo file
	topo_spec=open("interf_top.dat","w")
	a=0
	
		
	for j in range(-75,75):
		a+=1
		for i in range(0,len(alt_sampled)):
			if (j+75)<20:
				topo_spec.write("%lf\n"%(alt_sampled[i]*math.sin((j+75)/40.*math.pi)))
			elif(75-j)<20:
				topo_spec.write("%lf\n"%(alt_sampled[i]*math.sin((75-j)/40.*math.pi)))
			else:
				topo_spec.write("%lf\n"%(alt_sampled[i]))
			
	topo_spec.close()
	
	print("ydir=%d, xdir = %d\n"%(a,len(alt_sampled)))
	print("alt_max = %lf"%(max(alt_sampled)/1000.0))
		
	
