#################################################################
#                                                               #
#                                                               #
#                    Domain for specfem                         #
#                                                               #
#               meshing Chunk with meshfem                      #
#                                                               #
#                                                               #
#                                                               #
#################################################################

#----------------------------------------------------------------
# Important : the following format is used :
#
#  KEYNAME : VALUE
#        >- -< space on both sides 
#  need to have space before ":" and also after ":"
#  otherwise the VAKUE will wrong
#
#  line begins with "#" is a commment and will not
#  take into account 
#----------------------------------------------------------------

# -----------------------------------------------------
#  Domain in spherical coordiantes
#  since axisem use geocentric coordiantes 
#  we assume that the following values are
#  given in geocentric coordinates.

# center of domain lat, lon  (decimal degrees)
LATITUDE_CENTER_OF_CHUNK : 0.0
LONGITUDE_CENTER_OF_CHUNK : 35.0

# extention of chunk before rotation (decimal degrees)
LATITUDE_EXTENTION :  1.25
LONGITUDE_EXTENTION : 3.

# vertical size (km) 
VERTICAL_EXTENTION : 140.

# rotation of domain with respect to z axis in the center of domain 
# given with respect to the north (0. means no rotation)
AZIMUTH_OF_CHUNK : 0.

# depth for buried box (km)
# set 0. for domain that reach the free surface of the earth 
DEPTH_OF_CHUNK : 0.


#-------------------------------------------------------------
#
# etopo1 path  (comment to unuse it)
#
#ETOPO1 : /Data2/vmont/COUPLING_AXISEM_SPECFEM/EXAMPLE_COUPLING/Topo_files/etopo1_bed_g_f4.flt


#------------------------------------------------------------
# discretization of domain
#

#  in km 
SIZE_EL_LONGITUDE_IN_KM : 10. 
SIZE_EL_LATITUDE_IN_KM  : 10. 

# or in degrees
#SIZE_EL_LONGITUDE_IN_DEG : 0.9  
#SIZE_EL_LATITUDE_IN_DEG  : 0.9

# depth
SIZE_EL_DEPTH_IN_KM : 10.

# doubling --------------
#
# TODO
#

#################################################################
#                                                               #
#                                                               #
#                    AXISEM CONFIG                              #
#                                                               #
#                                                               #
#################################################################
#--------------------------------------------------------
#
# background model used in axisem : ak135, iasp91, prem  
#
#

#### BACKGROUND MODELS
EARTH_MODEL : ak135

# Dominant period [s]
DOMINANT_PERIOD : 15


# MPI decomposition for AXISEM
NTHETA_SLICES :  8
NRADIAL_SLICES : 1 

#################################################################
#                                                               #
#                                                               #
#                 SOURCES - RECEIVERS                           #
#                                                               #
#                                                               #
#################################################################

# path to CMT solution file (format from specfem)
CMT_SOLUTION_FILE : ./in_files/CMTSOLUTION_ev1

# path to station file (format :
# station_name  network_name latitue(deg) longitude(deg) elevation(m) buried(m)
STATION_FILE : ./in_files/STATIONS

#################################################################
#                                                               #
#                                                               #
#                   SPECFEM CONFIG                              #
#                                                               #
#                                                               #
#################################################################

# MPI decomposition for SPECFEM
NPROC : 8

# simulation time sampling 
NSTEP : 2000
DT : 0.05


#################################################################
#                                                               #
#                                                               #
#                   COUPLING CONFIG                             #
#                                                               #
#                                                               #
#################################################################


TIME_BEGIN : 775
TIME_END   : 875

##################################################################################
#
#                                MODEL
#
##################################################################################

# switch on model 
model_type = fk

#   FK input model  -------------------------
# number of layer for fk
fk_nb_layer : 3

# list of layer : id rho, vp, vs, depth_begin  
fk_layer : 1 2720. 5800. 3460.     0
fk_layer : 2 2920. 6500. 3850. -20000.
fk_layer : 3 3500. 8000. 4480. -35000.


#  model for internal mesher meshfem3D --------------------- 
# materials : choose materials 
number_of_material : 4
# id, vp ,vs, rho, Qp, Aniso (vs=0 => acoustic media) 
material : 1 5800. 3460. 2720.  9999 0
material : 2 6500. 3850. 2920.  9999 0
material : 3 8060. 4530. 3423.  9999 0
material : 4 8289. 4517. 3423.  9999 0

# material domains : define domains with differents materials  (todo check consistancy with domain) 
number_of_domain : 4
# material id, xmin, xmax, ymin, ymax, zmin, zmax
domain : 1     -150000 150000    -150000 200000 -20000      0
domain : 2     -150000 150000    -150000 200000 -35000   -20000
domain : 3     -150000 150000    -150000 200000 -120000  -35000
domain : 4     -150000 150000    -150000 200000 -250000  -120000

#---------------------------------------------------------------





#################################################################
#                                                               #
#                                                               #
#                       PATHS                                   #
#                                                               #
#                                                               #
#################################################################
#-------
# PATH TO SPECFEM DIRECTORY
#SPECFEM_DIR : /home/vmont/Work/test_axisem/EXAMPLE_COUPLING//specfem3d
SPECFEM_DIR : /Data2/vmont/COUPLING_AXISEM_SPECFEM/EXAMPLE_COUPLING/specfem3d
#--

#################################################################
#                                                               #
#                                                               #
#                       SCRIPTS                                 #
#                                                               #
#                                                               #
#################################################################
#
# todo choice beteween script for different clusters
#
